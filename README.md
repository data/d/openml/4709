# OpenML dataset: Physical_Activity_Recognition_Dataset_Using_Five_Smartphones

https://www.openml.org/d/4709

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset of walking, running, sitting, standing, jogging, biking, walking upstairs and walking downstairs activities. 

http://ps.ewi.utwente.nl/Datasets.php - original link.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4709) of an [OpenML dataset](https://www.openml.org/d/4709). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4709/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4709/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4709/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

